package com.example.apppizzaria;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;


public class Lanches extends AppCompatActivity {
    private ListView Lanche;
    SQLiteDatabase dblanches;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lanches);
        try {
            ArrayList<String> lanches = new ArrayList<>();
            Lanche = findViewById(R.id. IDlanches);


            dblanches = openOrCreateDatabase("lanches", MODE_PRIVATE, null);
            dblanches.execSQL ("CREATE TABLE IF NOT EXISTS lanches(lanche VARCHAR (30), tipolanche VARCHAR (30), precolanche FLOAT (06))");
            dblanches.execSQL ("INSERT INTO lanches(lanche, tipolanche, precolanche) VALUES ('X-Tudo' , 'Quente' , 10.50)");
            dblanches.execSQL ("INSERT INTO lanches(lanche, tipolanche, precolanche) VALUES ('X-Salada' , 'Quente' , 10.50)");
            dblanches.execSQL ("INSERT INTO lanches(lanche, tipolanche, precolanche) VALUES ('X-Egg' , 'Quente' , 10.50)");
            dblanches.execSQL ("INSERT INTO lanches(lanche, tipolanche, precolanche) VALUES ('Cachorro Quente' , 'Quente' , 10.50)");

            ArrayAdapter adptador3 = new ArrayAdapter<>(
                    getApplicationContext(),
                    android.R.layout.simple_list_item_1,
                    android.R.id.text1,
                    lanches);
            Lanche.setAdapter(adptador3);

            Cursor ponteiro = dblanches.rawQuery("SELECT * FROM lanches", null);
            int indicenomelanche = ponteiro.getColumnIndex("lanche");
           // int indicetipolanche = ponteiro.getColumnIndex("tipolanche");
            //int indiceprecolanche = ponteiro.getColumnIndex("precolanche");

            ponteiro.moveToFirst();

            while (ponteiro != null) {
                String nomeslanches = ponteiro.getString(indicenomelanche);
                //String tiposlanches = ponteiro.getString(indicetipolanche);
                //String precoslanches = ponteiro.getString(indiceprecolanche);
                lanches.add(nomeslanches);

                ponteiro.moveToNext();
            }

        }catch (Exception a){

            a.printStackTrace();

          }
        }

    }

