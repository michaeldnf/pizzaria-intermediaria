package com.example.apppizzaria;
import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;

public class Pizzas extends AppCompatActivity {
    private ListView nomepizza;
    SQLiteDatabase dbpizzas;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizzas);
        try {
            ArrayList<String> pizzas = new ArrayList<String>();
            nomepizza = findViewById(R.id.IDpizzas);


            dbpizzas = openOrCreateDatabase("pizzas", MODE_PRIVATE, null);
            dbpizzas.execSQL("CREATE TABLE IF NOT EXISTS pizzas(pizza VARCHAR(30), tamanhopizza VARCHAR(02), precopizza FLOAT(06))");
            dbpizzas.execSQL("INSERT INTO pizzas(pizza, tamanhopizza, precopizza) VALUES ('Marguerita', 'G', 46.90)");
            dbpizzas.execSQL("INSERT INTO pizzas(pizza, tamanhopizza, precopizza) VALUES ('Calabresa', 'G', 46.90)");
            dbpizzas.execSQL("INSERT INTO pizzas(pizza, tamanhopizza, precopizza) VALUES ('Baiana', 'G', 46.90)");

            ArrayAdapter adptador2 = new ArrayAdapter<>(
                    getApplicationContext(),
                    android.R.layout.simple_list_item_1,
                    android.R.id.text1,
                   pizzas
            );

            nomepizza.setAdapter(adptador2);

            Cursor ponteiro = dbpizzas.rawQuery("SELECT * FROM pizzas", null);
            int indicenomepizza = ponteiro.getColumnIndex("pizza");
            //int indicetamanhopizza = ponteiro.getColumnIndex("tamanhopizza");
            //int indiceprecopizza = ponteiro.getColumnIndex("precopizza");

            ponteiro.moveToFirst();

            while (ponteiro != null) {
                String pizza = ponteiro.getString(indicenomepizza);
                pizzas.add(pizza);

                ponteiro.moveToNext();
            }

        }catch (Exception a){

            a.printStackTrace();

        }
    }
}

