package com.example.apppizzaria;
import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import java.util.ArrayList;


public class Bebidas extends AppCompatActivity {

    SQLiteDatabase dbbebidas;
    private ListView listabebi;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bebidas);

        try {
            ArrayList<String> bebidas = new ArrayList<>();
            listabebi = findViewById(R.id.IDbebilist);

            dbbebidas = openOrCreateDatabase("bebidas", MODE_PRIVATE, null);
            dbbebidas.execSQL("CREATE TABLE IF NOT EXISTS bebida(nome VARCHAR(30), tipo VARCHAR(30), precobebi FLOAT(06))");
            dbbebidas.execSQL("INSERT INTO bebida(nome, tipo, precobebi) VALUES ('Coca-cola', 'refrigerante', 3.00)");
            dbbebidas.execSQL("INSERT INTO bebida(nome, tipo, precobebi) VALUES ('Fanta', 'refrigerante', 3.00)");
            dbbebidas.execSQL("INSERT INTO bebida(nome, tipo, precobebi) VALUES ('Sprit', 'refrigerante', 3.00)");
            dbbebidas.execSQL("INSERT INTO bebida(nome, tipo, precobebi) VALUES ('Skol', 'Cerveja', 5.00)");

            ArrayAdapter adptador = new ArrayAdapter<>(
                    getApplicationContext(),
                    android.R.layout.simple_list_item_1,
                    android.R.id.text1,
                    bebidas
            );

            listabebi.setAdapter(adptador);


            Cursor ponteiro = dbbebidas.rawQuery("SELECT * FROM bebida", null);
            int indicenome = ponteiro.getColumnIndex("nome");
            //int indicetipo = ponteiro.getColumnIndex("tipo");
            //int indiceprecobebi = ponteiro.getColumnIndex("precobebi");

            ponteiro.moveToFirst();

            while (ponteiro != null) {

                String nomes = ponteiro.getString(indicenome);
                //String tipos = ponteiro.getString(indicetipo);
               // String precobebidas = ponteiro.getString(indiceprecobebi);


                bebidas.add(nomes);



                ponteiro.moveToNext();

            }




        } catch (Exception a) {
            a.printStackTrace();
        }
    }
}
