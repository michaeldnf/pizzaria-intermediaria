package com.example.apppizzaria;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class Combos extends AppCompatActivity {
        private ListView nomecombo;
        SQLiteDatabase dbcombo;


        @SuppressLint("WrongViewCast")
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_combos);

            try {
            ArrayList<String> combos = new ArrayList<>();
            nomecombo = findViewById(R.id.IDcombo);


                dbcombo = openOrCreateDatabase("combo", MODE_PRIVATE, null);
                dbcombo.execSQL("CREATE TABLE IF NOT EXISTS combos(combo VARCHAR(30), tipo VARCHAR(30), preco FLOAT(06))");
                dbcombo.execSQL("INSERT INTO combos(combo, tipo, preco) VALUES ('X-sala/Suco', 'X-Salada/refri', 14.90)");
                dbcombo.execSQL("INSERT INTO combos(combo, tipo, preco) VALUES ('X-sala/Fritas', 'X-Salada/refri', 14.90)");
                dbcombo.execSQL("INSERT INTO combos(combo, tipo, preco) VALUES ('X-sala/Refri', 'X-Salada/refri', 14.90)");

                ArrayAdapter adptador4 = new ArrayAdapter<>(
                        getApplicationContext(),
                        android.R.layout.simple_list_item_1,
                        android.R.id.text1,
                        combos);
                nomecombo.setAdapter(adptador4);

                Cursor ponteiro = dbcombo.rawQuery("SELECT * FROM combos", null);
                int indicenomecombo = ponteiro.getColumnIndex("combo");
               // int indicetipocombo = ponteiro.getColumnIndex("tipo");
               // int indiceprecocombo = ponteiro.getColumnIndex("preco");

                ponteiro.moveToFirst();

                while (ponteiro != null) {
                    String nomescombo = ponteiro.getString(indicenomecombo);
                    //String tiposcombo = ponteiro.getString(indicetipocombo);
                    //String precoscombo = ponteiro.getString(indiceprecocombo);
                    combos.add(nomescombo);

                    ponteiro.moveToNext();
                }

            }catch (Exception a){

                a.printStackTrace();

            }
        }
    }